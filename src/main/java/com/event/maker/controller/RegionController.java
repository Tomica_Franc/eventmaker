package com.event.maker.controller;

import com.event.maker.model.Region;
import com.event.maker.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Autowired
    RegionService regionService;

    @GetMapping("/getAll")
    public List<Region> getAllRegions() {
        return regionService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Region getRegionById(@PathVariable final Integer id) {
        Optional<Region> optional = regionService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No region found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public Region getRegionByName(@PathVariable final String name) {
        return regionService.getRegionByName(name);
    }

    @GetMapping("/deleteRegion/{id}")
    public void deleteRegion(@PathVariable final Integer id) {
        Region region = regionService.getRegion(id);

        if(region != null) {
            regionService.deleteRegion(region);
        }
    }

    @PostMapping("/createRegion")
    public void createRegion(@RequestBody Region region) {
        if(region.getNaziv() == null) {
            throw new RuntimeException("Region does not have all arguments!");
        } else if(regionService.getRegionByName(region.getNaziv()) != null) {
            throw new RuntimeException("Region with same name already exists!");
        } else {
            regionService.createRegion(region);
        }
    }

    @PutMapping("/updateRegion/{id}")
    public void updateRegion(@RequestBody Region region, @PathVariable int id) {

        Optional<Region> optional = regionService.findById(id);
        Region currentRegion = optional.get();

        if (currentRegion != null) {
            currentRegion.setNaziv(region.getNaziv());

            regionService.createRegion(currentRegion);
        } else {
            throw new RuntimeException("Region does not exist!");
        }
    }

}
