package com.event.maker.service;

import com.event.maker.model.County;
import com.event.maker.repository.CountyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountyService {

    @Autowired
    CountyRepository countyRepository;

    public List<County> getAll() {
        return countyRepository.findAll();
    }

    public Optional<County> findById(Integer id) {
        return countyRepository.findById(id);
    }

    public County getCountyByName(String name) {
        return countyRepository.findByNaziv(name);
    }

    public County getCounty(Integer id) {
        return countyRepository.getOne(id);
    }

    public void deleteCounty(County county) {
        countyRepository.delete(county);
    }

    public void createCounty(County county) {
        countyRepository.save(county);
    }

    public List<County> getAllCountyByIdRegion(Integer id) {
        return countyRepository.findAllByIdRegije(id);
    }
}
