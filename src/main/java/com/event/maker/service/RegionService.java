package com.event.maker.service;

import com.event.maker.model.Region;
import com.event.maker.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegionService {

    @Autowired
    RegionRepository regionRepository;

    public List<Region> getAll() {
        return regionRepository.findAll();
    }

    public Optional<Region> findById(Integer id) {
        return regionRepository.findById(id);
    }

    public Region getRegionByName(String name) {
        return regionRepository.findByNaziv(name);
    }

    public Region getRegion(Integer id) {
        return regionRepository.getOne(id);
    }

    public void deleteRegion(Region region) {
        regionRepository.delete(region);
    }

    public void createRegion(Region region) {
        regionRepository.save(region);
    }
}
