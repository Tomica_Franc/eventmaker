package com.event.maker.model;

import javax.persistence.*;

@Entity
@Table(name = "velicina_grada", catalog = "event")
public class TownSize {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "velicina")
    private String velicina;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVelicina() {
        return velicina;
    }

    public void setVelicina(String velicina) {
        this.velicina = velicina;
    }
}
