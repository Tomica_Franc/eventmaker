package com.event.maker.repository;

import com.event.maker.model.County;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CountyRepository extends JpaRepository<County, Integer> {

    County findByNaziv(String name);

    List<County> findAllByIdRegije(Integer id);
}
