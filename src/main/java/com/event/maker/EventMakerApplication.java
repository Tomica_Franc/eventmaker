package com.event.maker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.event.maker.repository")
@SpringBootApplication
public class EventMakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventMakerApplication.class, args);
    }

}
