package com.event.maker.service;

import com.event.maker.model.Town;
import com.event.maker.repository.TownRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TownService {

    @Autowired
    TownRepository townRepository;

    public List<Town> getAll() {
        return townRepository.findAll();
    }

    public Optional<Town> findById(Integer id) {
        return townRepository.findById(id);
    }

    public Town getTownByName(String name) {
        return townRepository.findByNaziv(name);
    }

    public Town getTown(Integer id) {
        return townRepository.getOne(id);
    }

    public void deleteTown(Town town) {
        townRepository.delete(town);
    }

    public void createTown(Town town) {
        townRepository.save(town);
    }

    public List<Town> getTownByTownSize(Integer townSize) {
        return townRepository.findAllByIdVelicineGrada(townSize);
    }

    public List<Town> getTownByCounty(Integer countyId) {
        return townRepository.findAllByIdZupanije(countyId);
    }

    public List<Town> getTownsByCountyIds(List<Integer> countyIds) {
        return townRepository.findByIdZupanijeIn(countyIds);
    }
}
