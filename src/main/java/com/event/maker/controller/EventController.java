package com.event.maker.controller;

import com.event.maker.model.County;
import com.event.maker.model.Event;
import com.event.maker.model.Town;
import com.event.maker.service.CountyService;
import com.event.maker.service.EventService;
import com.event.maker.service.OrganizerService;
import com.event.maker.service.TownService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/event")
public class EventController {

    @Autowired
    EventService eventService;

    @Autowired
    TownService townService;

    @Autowired
    OrganizerService organizerService;

    @Autowired
    CountyService countyService;

    @GetMapping("/getAll")
    public List<Event> getAllEvents() {
        return eventService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Event getEventById(@PathVariable final Integer id) {
        Optional<Event> optional = eventService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No event found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public Event getEventByName(@PathVariable final String name) {
        return eventService.getEventByName(name);
    }

    @GetMapping("/getEventsByTown/{townId}")
    public List<Event> getEventsByTown(@PathVariable final Integer townId) {
        List<Event> events = eventService.getEventsByTown(townId);

        return events;
    }

    @GetMapping("/getEventsByOrganizer/{organizerId}")
    public List<Event> getEventsByOrganizer(@PathVariable final Integer organizerId) {
        List<Event> events = eventService.getEventsByOrganizer(organizerId);

        return events;
    }

    @GetMapping("/getEventsByEntrance/{entrance}")
    public List<Event> getEventsByEntrance(@PathVariable final String entrance) {
        List<Event> events = eventService.getEventsByEntrance(entrance);

        return events;
    }

    @GetMapping("/getEventsByTownSize/{townSize}")
    public List<Event> getEventsByTownSize(@PathVariable final Integer townSize) {
        List<Town> towns = townService.getTownByTownSize(townSize);

        List<Integer> townIds = towns
                .stream()
                .map(Town::getId)
                .collect(Collectors.toList());

        List<Event> events = eventService.getEventsByTownIds(townIds);

        return events;
    }

    @GetMapping("/getEventsByRegion/{regionId}")
    public List<Event> getEventsByRegion(@PathVariable final Integer regionId) {
        List<County> counties = countyService.getAllCountyByIdRegion(regionId);

        List<Integer> countyIds = counties.
                stream().
                map(County::getId).
                collect(Collectors.toList());

        List<Town> towns = townService.getTownsByCountyIds(countyIds);

        List<Integer> townIds = towns.
                stream().
                map(Town::getId).
                collect(Collectors.toList());

        List<Event> events = eventService.getEventsByTownIds(townIds);

        return events;
    }

    @GetMapping("/getEventsByCounty/{countyId}")
    public List<Event> getEventsByCounts(@PathVariable final Integer countyId) {
        List<Town> towns = townService.getTownByCounty(countyId);

        List<Integer> townIds = towns.
                stream().
                map(Town::getId).
                collect(Collectors.toList());

        List<Event> events = eventService.getEventsByTownIds(townIds);

        return events;
    }

    @GetMapping("/deleteEvent/{id}")
    public void deleteEvent(@PathVariable final Integer id) {
        Event event = eventService.getEvent(id);

        if(event != null) {
            eventService.deleteEvent(event);
        }
    }

    @PostMapping("/createEvent")
    public void createEvent(@RequestBody Event event) {
        if(event.getNaziv() == null || event.getVrijemeOd() == null || event.getVrijemeDo() == null ||
                event.getIdGrada() == 0 || event.getIdOrganizatora() == 0 || event.getSlobodanUlaz() == null) {
            throw new RuntimeException("Event does not have all arguments!");
        } else if(eventService.getEventByName(event.getNaziv()) != null) {
            throw new RuntimeException("Event with same name already exists!");
        } else {
            if(checkIfTownExist(event.getIdGrada()) == false) {
                throw new RuntimeException("Town does not exist!");
            }
            if(checkIfOrganizerExist(event.getIdOrganizatora()) == false) {
                throw new RuntimeException("Organizer does not exist!");
            }

            eventService.createEvent(event);
        }
    }

    /**
     * Returns true id organizer exists
     * @param idOrganizatora
     * @return
     */
    private boolean checkIfOrganizerExist(int idOrganizatora) {
        if(organizerService.getOrganizer(idOrganizatora) != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returs true if town exist
     * @param idGrada
     * @return
     */
    private boolean checkIfTownExist(int idGrada) {
        if(townService.getTown(idGrada) != null) {
            return true;
        } else {
            return false;
        }
    }

    @PutMapping("/updateEvent/{id}")
    public void updateEvent(@RequestBody Event event, @PathVariable int id) {

        Optional<Event> optional = eventService.findById(id);
        Event currentEvent = optional.get();

        if (currentEvent != null) {
            if(event.getNaziv() != null) {
                if(eventService.getEventByName(event.getNaziv()) != null) {
                    throw new RuntimeException("Event with same name already exist. Choose new name.");
                }
                currentEvent.setNaziv(event.getNaziv());
            }
            if(event.getVrijemeOd() != null)
                currentEvent.setVrijemeOd(event.getVrijemeOd());
            if(event.getVrijemeDo() != null)
                currentEvent.setVrijemeDo(event.getVrijemeDo());
            if(event.getIdGrada() != 0) {
                if(townService.getTown(event.getIdGrada()) == null) {
                    throw new RuntimeException("Town does not exist!");
                }
                currentEvent.setIdGrada(event.getIdGrada());
            }
            if(event.getIdOrganizatora() != 0) {
                if(organizerService.getOrganizer(event.getIdOrganizatora()) == null) {
                    throw new RuntimeException("Organizer does not exist!");
                }
                currentEvent.setIdOrganizatora(event.getIdOrganizatora());
            }
            if(event.getSlobodanUlaz() != null)
                currentEvent.setSlobodanUlaz(event.getSlobodanUlaz());

            eventService.createEvent(currentEvent);
        } else {
            throw new RuntimeException("Event does not exist!");
        }
    }

    @GetMapping("/search")
    public List<Event> findAll(@RequestParam Optional<String> name) {
        return eventService.findAll(name.orElse("_"));
    }
}
