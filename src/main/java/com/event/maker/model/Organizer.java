package com.event.maker.model;

import javax.persistence.*;

@Entity
@Table(name = "organizator", catalog = "event")
public class Organizer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "naziv")
    private String naziv;

    @Column(name = "pravo_organiziranja")
    private String pravoOrganiziranja;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getPravoOrganiziranja() {
        return pravoOrganiziranja;
    }

    public void setPravoOrganiziranja(String pravoOrganiziranja) {
        this.pravoOrganiziranja = pravoOrganiziranja;
    }
}
