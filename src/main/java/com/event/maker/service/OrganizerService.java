package com.event.maker.service;

import com.event.maker.model.Organizer;
import com.event.maker.repository.OrganizerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrganizerService {

    @Autowired
    OrganizerRepository organizerRepository;

    public List<Organizer> getAll() {
        return organizerRepository.findAll();
    }

    public Optional<Organizer> findById(Integer id) {
        return organizerRepository.findById(id);
    }

    public Organizer getOrganizerByName(String name) {
        return organizerRepository.findByNaziv(name);
    }

    public Organizer getOrganizer(Integer id) {
        return organizerRepository.getOne(id);
    }

    public void deleteOrganizer(Organizer organizer) {
        organizerRepository.delete(organizer);
    }

    public void createOrganizer(Organizer organizer) {
        organizerRepository.save(organizer);
    }
}
