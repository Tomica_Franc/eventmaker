package com.event.maker.model;

import javax.persistence.*;

@Entity
@Table(name = "zupanija", catalog = "event")
public class County {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "naziv")
    private String naziv;

    @Column(name = "id_regije")
    private int idRegije;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getIdRegije() {
        return idRegije;
    }

    public void setIdRegije(int idRegije) {
        this.idRegije = idRegije;
    }
}