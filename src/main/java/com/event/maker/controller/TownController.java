package com.event.maker.controller;

import com.event.maker.model.Town;
import com.event.maker.service.CountyService;
import com.event.maker.service.TownService;
import com.event.maker.service.TownSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/town")
public class TownController {

    @Autowired
    TownService townService;

    @Autowired
    TownSizeService townSizeService;

    @Autowired
    CountyService countyService;

    @GetMapping("/getAll")
    public List<Town> getAllTownSizes() {
        return townService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Town getTownById(@PathVariable final Integer id) {
        Optional<Town> optional = townService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No town found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public Town getTownByName(@PathVariable final String name) {
        return townService.getTownByName(name);
    }

    @GetMapping("/getAllTownsInSameCounty/{countyId}")
    public List<Town> getAllTownsInSameCounty(@PathVariable final Integer countyId) {
        return townService.getTownByCounty(countyId);
    }

    @GetMapping("/getAllWithTownSize/{townSize}")
    public List<Town> getAllTownsWithSize(@PathVariable final Integer townSize) {
        List<Town> towns = townService.getTownByTownSize(townSize);

        return towns;
    }

    @GetMapping("/deleteTown/{id}")
    public void deleteTown(@PathVariable final Integer id) {
        Town town = townService.getTown(id);

        if(town != null) {
            townService.deleteTown(town);
        }
    }

    @PostMapping ("/createTown")
    public void createTown(@RequestBody Town town) {
        if(town.getNaziv() == null || town.getIdZupanije() == 0 || town.getIdVelicineGrada() == 0) {
            throw new RuntimeException("Town does not have all arguments!");
        } else if(townService.getTownByName(town.getNaziv()) != null) {
            throw new RuntimeException("Town with same name already exists!");
        } else {
            if(checkIfTownSizeExist(town.getIdVelicineGrada()) == false) {
                throw new RuntimeException("Town size does not exist!");
            } else {
                townService.createTown(town);
            }
        }
    }

    /**
     * Check if town size exist, if it exist return true
     * @param id_velicine_grada
     * @return
     */
    private boolean checkIfTownSizeExist(Integer id_velicine_grada) {
        if(townSizeService.getTownSize(id_velicine_grada) != null) {
            return true;
        } else {
            return false;
        }
    }

    @PutMapping("/updateTown/{id}")
    public void updateTown(@RequestBody Town town, @PathVariable int id) {

        Optional<Town> optional = townService.findById(id);
        Town currentTown = optional.get();

        if (currentTown != null) {
            if(town.getNaziv() != null) currentTown.setNaziv(town.getNaziv());
            if(town.getIdVelicineGrada() != null) {
                if(checkIfTownSizeExist(town.getIdVelicineGrada()) == false) {
                    throw new RuntimeException("Town size does not exist!");
                } else {
                    currentTown.setIdVelicineGrada(town.getIdVelicineGrada());
                }
            }
            if(town.getIdZupanije() != null) {
                if(checkIfCountyExist(town.getIdZupanije()) == false) {
                    throw new RuntimeException(("County does not exist!"));
                } else {
                    currentTown.setIdZupanije(town.getIdZupanije());
                }
            }

            townService.createTown(currentTown);
        } else {
            throw new RuntimeException("Town does not exist!");
        }
    }

    /**
     * Return true if county exists
     * @param id_zupanije
     * @return
     */
    private boolean checkIfCountyExist(Integer id_zupanije) {
        if(countyService.getCounty(id_zupanije) != null) {
            return true;
        } else {
            return false;
        }
    }

}
