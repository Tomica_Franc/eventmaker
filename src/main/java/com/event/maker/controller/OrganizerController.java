package com.event.maker.controller;

import com.event.maker.model.Organizer;
import com.event.maker.service.OrganizerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/organizer")
public class OrganizerController {

    @Autowired
    OrganizerService organizerService;

    @GetMapping("/getAll")
    public List<Organizer> getAllOrganizers() {
        return organizerService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Organizer getOrganizerById(@PathVariable final Integer id) {
        Optional<Organizer> optional = organizerService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No organizer found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public Organizer getOrganizerByName(@PathVariable final String name) {
        return organizerService.getOrganizerByName(name);
    }

    @GetMapping("/deleteOrganizer/{id}")
    public void deleteOrganizer(@PathVariable final Integer id) {
        Organizer organizer = organizerService.getOrganizer(id);

        if(organizer != null) {
            organizerService.deleteOrganizer(organizer);
        }
    }

    @PostMapping("/createOrganizer")
    public void createOrganizer(@RequestBody Organizer organizer) {
        if(organizer.getNaziv() == null) {
            throw new RuntimeException("Organizer does not have all arguments!");
        } else if(organizerService.getOrganizerByName(organizer.getNaziv()) != null) {
            throw new RuntimeException("Organizer with same name already exists!");
        } else {
            if(organizer.getPravoOrganiziranja().isEmpty()) {
                organizer.setPravoOrganiziranja("NE");
            }
            organizerService.createOrganizer(organizer);
        }
    }

    @PutMapping("/updateOrganizer/{id}")
    public void updateOrganizer(@RequestBody Organizer organizer, @PathVariable int id) {

        Optional<Organizer> optional = organizerService.findById(id);
        Organizer currentOrganizer = optional.get();

        if (currentOrganizer != null) {
            if(organizer.getNaziv() != null) {
                currentOrganizer.setNaziv(organizer.getNaziv());
            }

            organizerService.createOrganizer(currentOrganizer);
        } else {
            throw new RuntimeException("Organizer does not exist!");
        }
    }
}
