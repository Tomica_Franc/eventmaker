package com.event.maker.controller;

import com.event.maker.model.County;
import com.event.maker.service.CountyService;
import com.event.maker.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/county")
public class CountyController {

    @Autowired
    CountyService countyService;

    @Autowired
    RegionService regionService;

    @GetMapping("/getAll")
    public List<County> getAllCounty() {
        return countyService.getAll();
    }

    @GetMapping("/getById/{id}")
    public County getCountyById(@PathVariable final Integer id) {
        Optional<County> optional = countyService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("No County found with given id!"));
    }

    @GetMapping("/getByName/{name}")
    public County getCountyByName(@PathVariable final String name) {
        return countyService.getCountyByName(name);
    }

    @GetMapping("/getAllCountyByIdRegion/{idRegije}")
    public List<County> getAllCountyByIdRegion(@PathVariable final Integer idRegije) {
        return countyService.getAllCountyByIdRegion(idRegije);
    }

    @GetMapping("/deleteCounty/{id}")
    public void deleteCounty(@PathVariable final Integer id) {
        County county = countyService.getCounty(id);

        if(county != null) {
            countyService.deleteCounty(county);
        }
    }

    @PostMapping("/createCounty")
    public void createCounty(@RequestBody County county) {
        if(county.getNaziv() == null || county.getIdRegije() == 0) {
            throw new RuntimeException("County does not have all arguments!");
        } else if(countyService.getCountyByName(county.getNaziv()) != null) {
            throw new RuntimeException("County with same name already exists!");
        } else {
            if(county.getIdRegije() != 0 || county.getIdRegije() == 0) {
                if(checkIfRegionExist(county.getIdRegije()) == false) {
                    throw new RuntimeException("Region with given id does not exist");
                }
            }

            countyService.createCounty(county);
        }
    }

    /**
     * Returns true if region does not exist
     * @param idRegije
     * @return
     */
    private boolean checkIfRegionExist(int idRegije) {
        if(regionService.getRegion(idRegije) == null) {
            return false;
        } else {
            return true;
        }
    }

    @PutMapping("/updateCounty/{id}")
    public void updateCounty(@RequestBody County county, @PathVariable int id) {

        Optional<County> optional = countyService.findById(id);
        County currentCounty = optional.get();

        if (currentCounty != null) {
            if(county.getNaziv() != null)
                currentCounty.setNaziv(county.getNaziv());
            if(county.getIdRegije() != 0 || county.getIdRegije() == 0) {
                if(checkIfRegionExist(county.getIdRegije()) == false) {
                    throw new RuntimeException("Region does not exist!");
                } else {
                    currentCounty.setIdRegije(county.getIdRegije());
                }
            }

            countyService.createCounty(currentCounty);
        } else {
            throw new RuntimeException("County does not exist!");
        }
    }
}
