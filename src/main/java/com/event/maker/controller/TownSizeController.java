package com.event.maker.controller;

import com.event.maker.model.TownSize;
import com.event.maker.service.TownSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/townSize")
public class TownSizeController {

    @Autowired
    TownSizeService townSizeService;

    @GetMapping("/getAll")
    public List<TownSize> getAllTownSizes() {
        return townSizeService.getAll();
    }

    @GetMapping("/getById/{id}")
    public TownSize getTownSizeById(@PathVariable final Integer id) {
        Optional<TownSize> optional = townSizeService.findById(id);

        return optional.orElseThrow(() -> new RuntimeException("Town size not found with given id!"));
    }

    @GetMapping("/getByVelicina/{velicina}")
    public TownSize getTownSizeByName(@PathVariable final String velicina) {
        return townSizeService.getTownSizeByVelicina(velicina);
    }

    @GetMapping("/deleteTownSize/{id}")
    public void deleteTownSize(@PathVariable final Integer id) {
        TownSize townSize = townSizeService.getTownSize(id);

        if(townSize != null) {
            townSizeService.deleteTownSize(townSize);
        }
    }

    @PostMapping("/createTownSize")
    public void createTownSize(@RequestBody TownSize townSize) {
        if(townSize.getVelicina() == null) {
            throw new RuntimeException("TownSize does not have all arguments!");
        } else if(townSizeService.getTownSizeByVelicina(townSize.getVelicina()) != null) {
            throw new RuntimeException("TownSize with same size already exists!");
        } else {
            townSizeService.createTownSize(townSize);
        }
    }

    @PutMapping("/updateTownSize/{id}")
    public void updateTownSize(@RequestBody TownSize townSize, @PathVariable int id) {

        Optional<TownSize> optional = townSizeService.findById(id);
        TownSize currentTownSize = optional.get();

        if (currentTownSize != null) {
            if(townSize.getVelicina() != null) {
                currentTownSize.setVelicina(townSize.getVelicina());
            }
            townSizeService.createTownSize(currentTownSize);
        } else {
            throw new RuntimeException("Town does not exist!");
        }
    }

}
