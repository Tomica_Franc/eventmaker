package com.event.maker.model;

import javax.persistence.*;

@Entity
@Table(name = "grad", catalog = "event")
public class Town {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "naziv")
    private String naziv;

    @Column(name = "id_velicine_grada")
    private Integer idVelicineGrada;

    @Column(name = "id_zupanije")
    private Integer idZupanije;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Integer getIdVelicineGrada() {
        return idVelicineGrada;
    }

    public void setIdVelicineGrada(Integer idVelicineGrada) {
        this.idVelicineGrada = idVelicineGrada;
    }

    public Integer getIdZupanije() {
        return idZupanije;
    }

    public void setIdZupanije(Integer idZupanije) {
        this.idZupanije = idZupanije;
    }
}
