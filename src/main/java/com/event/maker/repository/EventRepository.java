package com.event.maker.repository;

import com.event.maker.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Integer> {

    Event findByNaziv(String name);

    List<Event> findAllByIdGrada(Integer townId);

    List<Event> findAllByIdOrganizatora(Integer organizerId);

    List<Event> findByIdGradaIn(List<Integer> townIds);

    @Query("select e from Event e where naziv like %?1%")
    List<Event> getAllByNaziv(String name);

    List<Event> findAllBySlobodanUlaz(String entrance);
}
