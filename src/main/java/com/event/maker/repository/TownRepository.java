package com.event.maker.repository;

import com.event.maker.model.Town;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TownRepository extends JpaRepository<Town, Integer> {

    Town findByNaziv(String name);

    List<Town> findAllByIdVelicineGrada(Integer townSize);

    List<Town> findAllByIdZupanije(Integer countyId);

    List<Town> findByIdZupanijeIn(List<Integer> countyIds);
}
