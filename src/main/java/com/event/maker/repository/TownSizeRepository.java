package com.event.maker.repository;

import com.event.maker.model.TownSize;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TownSizeRepository extends JpaRepository<TownSize, Integer> {

    TownSize findByVelicina(String name);
}
