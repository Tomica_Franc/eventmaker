package com.event.maker.repository;

import com.event.maker.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Integer> {

    Region findByNaziv(String name);
}
