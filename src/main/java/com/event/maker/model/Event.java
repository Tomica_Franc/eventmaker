package com.event.maker.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "dogadjaj", catalog = "event")
public class Event {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "naziv")
    private String naziv;

    @Column(name = "vrijeme_od")
    private Timestamp vrijemeOd;

    @Column(name = "vrijeme_do")
    private Timestamp vrijemeDo;

    @Column(name = "slobodan_ulaz")
    private String slobodanUlaz;

    @Column(name = "id_grada")
    private int idGrada;

    @Column(name = "id_organizatora")
    private int idOrganizatora;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Timestamp getVrijemeOd() {
        return vrijemeOd;
    }

    public void setVrijemeOd(Timestamp vrijemeOd) {
        this.vrijemeOd = vrijemeOd;
    }

    public Timestamp getVrijemeDo() {
        return vrijemeDo;
    }

    public void setVrijemeDo(Timestamp vrijemeDo) {
        this.vrijemeDo = vrijemeDo;
    }

    public String getSlobodanUlaz() {
        return slobodanUlaz;
    }

    public void setSlobodanUlaz(String slobodanUlaz) {
        this.slobodanUlaz = slobodanUlaz;
    }

    public int getIdGrada() {
        return idGrada;
    }

    public void setIdGrada(int idGrada) {
        this.idGrada = idGrada;
    }

    public int getIdOrganizatora() {
        return idOrganizatora;
    }

    public void setIdOrganizatora(int idOrganizatora) {
        this.idOrganizatora = idOrganizatora;
    }
}
