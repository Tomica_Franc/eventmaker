package com.event.maker.repository;

import com.event.maker.model.Organizer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizerRepository extends JpaRepository<Organizer, Integer> {

    Organizer findByNaziv(String name);
}
