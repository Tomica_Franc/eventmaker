package com.event.maker.service;
import com.event.maker.model.TownSize;
import com.event.maker.repository.TownSizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class TownSizeService {

    @Autowired
    TownSizeRepository townSizeRepository;

    public List<TownSize> getAll() {
        return townSizeRepository.findAll();
    }

    public Optional<TownSize> findById(Integer id) {
        return townSizeRepository.findById(id);
    }

    public TownSize getTownSizeByVelicina(String name) {
        return townSizeRepository.findByVelicina(name);
    }

    public TownSize getTownSize(Integer id) {
        return townSizeRepository.getOne(id);
    }

    public void deleteTownSize(TownSize townSize) {
        townSizeRepository.delete(townSize);
    }

    public void createTownSize(TownSize townSize) {
        townSizeRepository.save(townSize);
    }
}
