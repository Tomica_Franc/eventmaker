package com.event.maker.service;

import com.event.maker.model.Event;
import com.event.maker.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    public Optional<Event> findById(Integer id) {
        return eventRepository.findById(id);
    }

    public Event getEventByName(String name) {
        return eventRepository.findByNaziv(name);
    }

    public Event getEvent(Integer id) {
        return eventRepository.getOne(id);
    }

    public void deleteEvent(Event event) {
        eventRepository.delete(event);
    }

    public void createEvent(Event event) {
        eventRepository.save(event);
    }

    public List<Event> getEventsByTown(Integer townId) {
        return eventRepository.findAllByIdGrada(townId);
    }

    public List<Event> getEventsByOrganizer(Integer organizerId) {
        return eventRepository.findAllByIdOrganizatora(organizerId);
    }

    public List<Event> getEventsByTownIds(List<Integer> townIds) {
        return eventRepository.findByIdGradaIn(townIds);
    }

    public List<Event> findAll(String name) {
        return eventRepository.getAllByNaziv(name);
    }

    public List<Event> getEventsByEntrance(String entrance) {
        return eventRepository.findAllBySlobodanUlaz(entrance);
    }
}
